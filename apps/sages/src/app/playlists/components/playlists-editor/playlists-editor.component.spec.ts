import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { ComponentFixture, fakeAsync, flushMicrotasks, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormsModule, NgModel } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { playlistsMock } from 'apps/sages/src/stories/playlists/playlistsMock';

import { PlaylistsEditorComponent } from './playlists-editor.component';
// import '@angular/material/prebuilt-themes/indigo-pink.css';

describe('PlaylistsEditorComponent', () => {
  let component: PlaylistsEditorComponent;
  let fixture: ComponentFixture<PlaylistsEditorComponent>;
  let elem: DebugElement

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PlaylistsEditorComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        NoopAnimationsModule,
        FormsModule,
        MatFormFieldModule,
        MatInputModule
      ]
    })
      .compileComponents();
  });

  beforeAll(() => {
    document.body.append(`<style>
    .mat-theme-loaded-marker {
      display: none;
    }</style>`)
    document.body.classList.add('mat-theme-loaded-marker')
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistsEditorComponent);
    component = fixture.componentInstance;
    elem = fixture.debugElement
    component.playlist = playlistsMock[1]
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fill form with playlist data', async () => {
    await fixture.whenStable()

    // fixture.detectChanges();
    const fields = elem.queryAll(By.directive(NgModel))
    const namefield = fields.find(m => m.attributes.name === 'name')

    expect(namefield!.nativeElement.value).toMatch(playlistsMock[1].name)
  });
  it('should update counter when name changes', fakeAsync(() => {
    // await fixture.whenStable()

    tick()

    const counter = elem.query(By.css('[data-testid="counter"]'))
    const fields = elem.queryAll(By.directive(NgModel))
    const namefield = fields.find(m => m.attributes.name === 'name')!

    namefield.nativeElement.value = 'Placki'
    // namefield.triggerEventHandler('input', {target: namefield.nativeElement})
    namefield.nativeElement.dispatchEvent(new Event('input'))
    tick()
    fixture.detectChanges()
    // flushMicrotasks()

    expect(counter.nativeElement.textContent).toMatch('6 / 170')
  }));
});
