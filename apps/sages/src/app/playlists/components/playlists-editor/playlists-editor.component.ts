import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Playlist } from '../../../core/model/Playlist';

@Component({
  selector: 'sages-playlists-editor',
  templateUrl: './playlists-editor.component.html',
  styleUrls: ['./playlists-editor.component.scss']
})
export class PlaylistsEditorComponent implements OnInit {

  /**
   * Playlist data
   */
  @Input() playlist: Playlist = {
    id: '',
    name: '',
    public: false,
    description: '',
  }

  @ViewChild('formRef', { read: NgForm })
  formRef?: NgForm

  @Output() onCancel = new EventEmitter();
  @Output() onSave = new EventEmitter();

  ngAfterViewInit(): void {
  }

  submit() {
    if (this.formRef?.invalid) { return }
    this.onSave.emit({
      ...this.playlist,
      ...this.formRef?.value,
    })
  }

  cancel() {
    this.onCancel.emit()
  }

  constructor() { }

  ngOnInit(): void {
  }

}
