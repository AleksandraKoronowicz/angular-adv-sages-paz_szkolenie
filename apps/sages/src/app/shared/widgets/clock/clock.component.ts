import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, NgZone, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'sages-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush // wait for push form template (input/event)
})
export class ClockComponent implements OnInit {
  time = '';

  @Input() somePushedData: any = ''; // triggers detectChanges()

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    // console.log('new input')
  }

  updateTime() {
    this.time = (new Date()).toLocaleTimeString()
  }

  constructor(
    private ngZone: NgZone,
    private cdr: ChangeDetectorRef) {
    // this.cdr.detach()
  }

  ngOnInit(): void {
    this.updateTime()
    // this.cdr.detectChanges()

    this.ngZone.runOutsideAngular(() => { // dont detect changes globaly
      setInterval(() => {
        this.updateTime()
        this.cdr.detectChanges() // local detection now
        // this.cdr.markForCheck() // schedule for next global detection
      }, 1000)
    })
  }

}
