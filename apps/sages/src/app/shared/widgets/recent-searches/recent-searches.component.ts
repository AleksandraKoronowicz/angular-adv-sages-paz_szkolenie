import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Inject } from '@angular/core';
import { filter, scan, startWith, tap } from 'rxjs/operators';
import { ISearchService } from '../../../browse-music/services/ISearchService';
import { MusicSearchService } from '../../../browse-music/services/music-search.service';
import { Album } from '../../../core/model/album';

@Component({
  selector: 'sages-recent-searches',
  templateUrl: './recent-searches.component.html',
  styleUrls: ['./recent-searches.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RecentSearchesComponent {

  queries = this.service.queryChange.pipe(
    filter(query => query !== ''),
    startWith('alice'),
    scan<string, string[]>((queries, query) => [...queries.slice(-4), query], [
      'bob','test'
    ])
  )

  constructor(
    @Inject(ISearchService)
    private service: ISearchService<Album, string>) { }

  search(query: string) {
    this.service.search(query)
  }

}
