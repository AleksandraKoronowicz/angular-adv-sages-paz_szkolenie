import { Playlist } from "../model/Playlist";

export namespace Playlists {

    export class Add {
        static readonly type = '[Playlist] Add';
        constructor(public payload: Playlist) { }
    }

    export class Update {
        static readonly type = '[Playlist] Update';
        constructor(public payload: Playlist) { }
    }

    export class FetchAll {
        static readonly type = '[Playlist] Fetch All';
    }

    export class Delete {
        static readonly type = '[Playlist] Delete';
        constructor(public id: Playlist['id']) { }
    }
}