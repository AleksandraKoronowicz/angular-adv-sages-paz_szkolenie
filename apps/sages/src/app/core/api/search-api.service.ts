import { HttpClient, HttpContext } from '@angular/common/http';
import { ErrorHandler, Inject, Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { Album, assertAlbumResponse, isAlbumResponse, SearchResponse } from '../model/album';
import { AuthService } from '../services/auth.service';
import { API_URL } from '../tokens';

@Injectable({
  providedIn: 'root'
})
export class SearchApiService {
  constructor(
    // private errorHandler: ErrorHandler, // Sentry / Prometheus / etc telemetry
    @Inject(API_URL) private api_url: string,
    private http: HttpClient
  ) { }

  fetchSearch(params: { query: string }) {
    // https://developer.spotify.com/documentation/web-api/reference/#category-search
    return this.http.get<SearchResponse<Album>>(`${this.api_url}/search`, {
      params: {
        type: 'album', ...params
      },
      // context: new HttpContext()
    })
  }

  fetchAlbumById(id: Album['id']) {
    return this.http.get(`https://api.spotify.com/v1/albums/${id}`).pipe(
      map(res => {
        assertAlbumResponse(res)
        return res
      })
    )
  }
}
