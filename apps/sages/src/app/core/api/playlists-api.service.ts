import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { PagingObject } from '../model/album';
import { Playlist } from '../model/Playlist';
import { API_URL } from '../tokens';

@Injectable({
  providedIn: 'root'
})
export class PlaylistsApiService {
  fetchPlaylistById(id: any) {
    return this.http.get<Playlist>(`${this.api_url}/playlists/${id}`)
  }

  constructor(
    @Inject(API_URL) private api_url: string,
    private http: HttpClient) { }

  fetchPlaylists() {
    return this.http.get<PagingObject<Playlist>>(`${this.api_url}/me/playlists`)
      .pipe(map(resp => resp.items))
  }


}
