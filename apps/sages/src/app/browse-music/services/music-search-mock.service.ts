import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { Album } from '../../core/model/album';
import { ISearchService } from "./ISearchService";

@Injectable({
  providedIn: 'root'
})
export class MusicSearchMockService implements ISearchService<Album, string>{

  constructor() { }

  resultsChange: Observable<Album[]> = this.search('')
  queryChange: Observable<string> = of('')

  search(query: string): Observable<Album[]> {
    return of([
      { id: '123', name: 'Album 123', type: 'album', images: [{ url: 'https://www.placecage.com/c/200/200' }] },
      { id: '234', name: 'Album 234', type: 'album', images: [{ url: 'https://www.placecage.com/c/300/300' }] },
      { id: '456', name: 'Album 456', type: 'album', images: [{ url: 'https://www.placecage.com/c/500/500' }] },
      { id: '567', name: 'Album 567', type: 'album', images: [{ url: 'https://www.placecage.com/c/400/400' }] },
    ] as Album[])//.pipe(delay(100))
  }
}
