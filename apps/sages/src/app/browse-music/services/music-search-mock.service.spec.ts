import { TestBed } from '@angular/core/testing';

import { MusicSearchMockService } from './music-search-mock.service';

describe('MusicSearchMockService', () => {
  let service: MusicSearchMockService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MusicSearchMockService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
