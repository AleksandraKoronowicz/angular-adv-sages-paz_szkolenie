import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Playlist } from './Playlist';

@Component({
  selector: 'ui-ui-widget',
  templateUrl: './ui-widget.component.html',
  styleUrls: ['./ui-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UiWidgetComponent implements OnInit {

  @Input() playlist?: Playlist;

  constructor() { }

  ngOnInit(): void {
  }

}
